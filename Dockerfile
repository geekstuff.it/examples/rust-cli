ARG APP_NAME="hello"

# Build stage
FROM rust:latest AS build

RUN rustup target add x86_64-unknown-linux-musl
RUN apt update && apt install -y musl-tools musl-dev
RUN update-ca-certificates

# Create app user
RUN adduser \
    --disabled-password \
    --gecos "" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "1000" \
    "app"

# Build
WORKDIR /app
COPY Cargo.* .
COPY src/ ./src/
RUN cargo build --target x86_64-unknown-linux-musl --release

# Final stage
FROM scratch

# Trickle down APP_NAME argument into this stage
ARG APP_NAME

# Import app user
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group

# Copy our built app
COPY --from=build /app/target/x86_64-unknown-linux-musl/release/${APP_NAME} /app

# Use app user
USER app:app

# Send every (or no) commands to our app
ENTRYPOINT ["/app"]
