# playground\rust-cli

Simple Rust example, backed by a devcontainer.

## How to use

- Clone this repo.
- Open it using VSCode.
- Install the (1) recommended extension, remote containers.
- Click popup to "Reopen in Container"
    - Could take a few minutes to build the first time.
- Open a new integrated terminal
- Type `make`, space, double TAB to see an auto-completed list of makefile targets to try!

## TODO

- [x] Use the toml way
- Try using breakpoints / debugging
- Create a CLI to print hello $name
- [x] Add Dockerfile from SCRATCH
- Find out if Auto-DevOps has something for Rust on the CI part
