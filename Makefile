#!make

MAKEFLAGS += --no-print-directory
BUILD_TARGET ?= x86_64-unknown-linux-musl
DOCKER_IMAGE ?= playground-rust-cli-hello

all: run

.PHONY: build
build:
	@echo "[] Build"
	@cargo build

.PHONY: run
run: build
	@echo "[] Run Rust App"
	@./target/debug/hello
	@$(MAKE) cleanup

.PHONY: cleanup
cleanup:
	@echo "[] Cleanup"
	@rm -rf ./target/debug

# TODO There is likely a better way than per-file.
.PHONY: fmt
fmt:
	@echo "[] Rust fmt"
	rustfmt src/main.rs

.PHONY: versions
versions:
	@rustc --version
	@cargo --version

.PHONY: docker
docker:
	@echo "[] Build Docker image (FROM scratch)"
	docker build -t ${DOCKER_IMAGE} .
	@docker images \
		--format 'Docker image created: {{.CreatedSince}}, size: {{.Size}}' \
		${DOCKER_IMAGE}

.PHONY: run-docker
run-docker: docker
	@echo "[] Docker run"
	docker run --rm ${DOCKER_IMAGE}
